package com.mycompany.ox;

import java.util.*;

public class OX {

    public static String showWelcome() {
        String showWel = "***** Welcome to OX GAME *****";
        return showWel;
    }

    public static void showTable(char[][] TABLE) {
        for (int i = 0; i < 3; i++) {
            System.out.println("-------------");
            for (int j = 0; j < 3; j++) {
                System.out.print("| ");
                System.out.print(TABLE[i][j] + " ");
            }
            System.out.print("|");
            System.out.println();
        }
        System.out.println("-------------");
    }

    public static String showTurn(char turn) {
        String showT = "Turn : " + turn;
        return showT;
    }

    public static void InputO(char[][] TABLE, int row, int col, int count,char turn) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Please input row and collum : ");
        row = kb.nextInt();
        col = kb.nextInt();
        addOX(TABLE, row, col, count,turn);
    }

    public static void addOX(char[][] TABLE, int row, int col, int count,char turn) {
        if (checkInputError(row, col) == true || checkRepeatError(TABLE, row, col) == true) {
            InputO(TABLE, row, col, count,turn);
        } else {
            TABLE[row][col] = turn;
        }
    }

    public static char changeTurn(char turn) {
        if (turn == 'O') {
            turn = 'X';
        } else {
            turn = 'O';
        }
        return turn;
    }

    public static boolean checkInputError(int row, int col) {
        if ((row > 2 || row < 0) && (col > 2 || col < 0)) {
            System.out.println("ERROR Row & Collum!!, Please input 0 or 1 or 2");
            return true;
        } else if (col > 2 || col < 0) {
            System.out.println("ERROR Collum!!, Please input 0 or 1 or 2");
            return true;
        } else if (row > 2 || row < 0) {
            System.out.println("ERROR Row!!, Please input 0 or 1 or 2");
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkRepeatError(char[][] TABLE, int row, int col) {
        if (TABLE[row][col] == 'O' || TABLE[row][col] == 'X') {
            System.out.println("Please choose another Row and Collum!!");
            return true;
        } else {
            return false;
        }
    }

    public static boolean checkWinRow(char[][] TABLE,char turn) {
        for (int r = 0; r < 3; r++) {
            if (TABLE[r][0] == turn && TABLE[r][1] == turn && TABLE[r][2] == turn) {
                showTable(TABLE);
                System.out.println(showWin(turn));
                System.out.println(showBye());
                return true;
            }
        }
        return false;
    }

    public static boolean checkWinCol(char[][] TABLE,char turn) {
        for (int r = 0; r < 3; r++) {
            if (TABLE[0][r] == turn && TABLE[1][r] == turn && TABLE[2][r] == turn) {
                showTable(TABLE);
                System.out.println(showWin(turn));
                System.out.println(showBye());
                return true;
            }
        }
        return false;
    }

    public static boolean checkWinDiagonalRight(char[][] TABLE,char turn) {
        if (TABLE[0][2] == turn && TABLE[1][1] == turn && TABLE[2][0] == turn) {
            showTable(TABLE);
            System.out.println(showWin(turn));
            System.out.println(showBye());
            return true;
        }
        return false;
    }

    public static boolean checkWinDiagonalLeft(char[][] TABLE,char turn) {
        if (TABLE[0][0] == turn && TABLE[1][1] == turn && TABLE[2][2] == turn) {
            showTable(TABLE);
            System.out.println(showWin(turn));
            System.out.println(showBye());
            return true;
        }
        return false;
    }

    public static boolean checkDraw(char[][] TABLE, int count) {
        if (count == 9) {
            showTable(TABLE);
            System.out.println("It is draw!!");
            System.out.println(showBye());
            return true;
        }
        return false;
    }

    public static String showWin(char turn) {
        String showW = "Player " + turn + " win!!";
        return showW;
    }

    public static String showBye() {
        String showB = "Bye Bye ,See you next time!!";
        return showB;
    }

    public static void main(String[] args) {
        char[][] TABLE = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        char turn = 'O';
        int row = 0, col = 0;
        int count = 0;
        System.out.println(showWelcome());
        for (;;) {
            showTable(TABLE);
            System.out.println(showTurn(turn));
            InputO(TABLE, row, col, count,turn);
            count++;
            
            if (checkWinRow(TABLE,turn) == true || checkWinCol(TABLE,turn) == true
                    || checkWinDiagonalRight(TABLE,turn) == true || checkWinDiagonalLeft(TABLE,turn) == true) {
                break;
            } else if (checkDraw(TABLE, count) == true) {
                break;
            }
            turn = changeTurn(turn);
        }
    }

}
